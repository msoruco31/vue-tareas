app.component('footer-banco',{
    props: ['valor','fecha'], //Comunicacion entre componentes, hace refernaic a dat de otro componente
    template: `<div class="bg-dark py-3 mt-2 text-white">
                    <h3>{{texto}} - {{valor}}</h3>
                    <p>{{fecha}}</p>
                </div>`,
    data(){
        return{
            texto: 'Footer de Mi sitio web de Banco dinamico'
        }

    }

})