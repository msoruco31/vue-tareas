const app = Vue.createApp({
    data(){
        return {
            titulo: "Banco Vue 3", 
            cantidad:200,
            enlace: 'http://www.google.cl',
            estado: true,
            servicios: ["trasnferencias", "pagos", "cajero", "cheques"]
        }
    },
    methods: {
        agregarSaldo(){
            this.cantidad += 100
        },
        quitarSaldo(valor){
            if(this.cantidad <= 0){
                alert('saldo en 0!!!');
                return;
            }
            this.cantidad = this.cantidad-valor
        }
    },
    computed:{
        colorCantidad(){
            return this.cantidad > 300 ? 'text-success' : 'text-danger'
        },
        upperTitulo(){
            return this.titulo.toUpperCase()
        }
    }
}) 