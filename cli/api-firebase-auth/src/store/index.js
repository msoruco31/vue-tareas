import { createStore } from 'vuex'
import router from '../router'

export default createStore({
  state: {
    tareas: [],
    tarea: {
      id: '',
      nombre: '',
      categorias: [],
      estado: '',
      numero: 0
    }
  },
  mutations: {
    cargar(state, payload){
      state.tareas = payload
    },
    set(state,payload){
      state.tareas.push(payload)
     
    },
    eliminar(state, payload){
      state.tareas = state.tareas.filter(tarea => tarea.id !== payload)
      
    },
    tarea(state, payload){
      if(!state.tareas.find(tarea => tarea.id === payload)){
        router.push('/')
        return;
      }
      state.tarea = state.tareas.find(tarea => tarea.id === payload)
    },
    update(state,payload){
      state.tareas = state.tareas.map(tarea => tarea.id === payload.id ? payload : tarea)
     
    }
  },
  actions: {
    async cargarTareas ({commit}){
      try {
        const res = await fetch(`https://api-tareas-b923d-default-rtdb.firebaseio.com/tareas.json`, {
          method: 'GET'          
        });
        const dataDB = await res.json();
        const arrTareas = [];
        console.log(dataDB);
        commit('cargar', arrTareas)
        
        for(let  id in dataDB){
          arrTareas.push(dataDB[id]);
        }

      }catch (error) {
       console.log(error); 
      }
      
    },
    async setTareas({commit}, tarea){
      try{
        const res = await fetch(`https://api-tareas-b923d-default-rtdb.firebaseio.com/tareas/${tarea.id}.json`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(tarea)
        });
        const dataDB = await res.json()
        console.log(dataDB);
      }catch(error){
        console.log(error);
      }
      commit('set', tarea)
    },
    async deleteTareas({commit}, id){
      try{
        const res = await fetch(`https://api-tareas-b923d-default-rtdb.firebaseio.com/tareas/${id}.json`, {
          method: 'DELETE'
        });       
        commit('eliminar',id)  
      }catch(error){
        console.log(error);
      }    
    },
    setTarea({commit}, id){
      commit('tarea',id)
    },
    async updateTarea({commit}, tarea){
      try{
        const res = await fetch(`https://api-tareas-b923d-default-rtdb.firebaseio.com/tareas/${tarea.id}.json`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(tarea)
        });
        const dataDB = await res.json();
        console.log(dataDB);
        commit('update', dataDB)        
      router.push('/')
      }catch(error){
        console.log(error);
      }
      
    }

  },
  modules: {
  }
})
