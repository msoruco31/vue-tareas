import { createStore } from 'vuex'

export default createStore({
  state: {
    contador: 150
  },
  mutations: {
    incrementar (state, payload) {
      state.contador += payload;
    },
    disminuir (state, payload) {
      state.contador -= payload;
    }
  },
  actions: {
    accionIncrementar({commit}){
      commit('incrementar',10);
    },
    accionDisminuir({commit}, numero){
      commit('disminuir', numero);
    },

    modificarContador ({commit}, obj){
      if(obj.estado){
        commit('incrementar', obj.numero)
      }else{
        commit('disminuir', obj.numero)
      }
    }
  },
  modules: {
  }
})
