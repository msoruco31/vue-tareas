//Fat Arrow Functions
const sumar = (num1,num2) => (num1+num2)
const resultado = sumar(10,5)
console.log(resultado)

const mensaje = nombre => ('hola soy ' + nombre);
console.log(mensaje('marco'));

const suma3 = (num = 0) => (num +3)
console.log(suma3())

//Template String -- incluye logica js
const mensajeNumero = num => (
	`El numero es:  ${num*2}`
 )
console.log(mensajeNumero(10)) 

//Objetos
const mascota = {
    nombre: 'Juanito',
    edad: 10,
    raza:'gato',
    vivo:true,
    comida:['whiskas','atun', 'ratones']
}
console.log(mascota);
console.log(mascota.nombre);
console.log(mascota.edad);
console.log(mascota.vivo);

mascota.id=1
mascota.comida[1]='Salmon'
console.log(mascota)

//Destructuring Objetos
const mascota = {
    nombre: 'Juanito',
    edad: 10,
    raza:'gato',
    vivo:true,
    comida:['whiskas','atun', 'ratones']
}
const nombreMascota = mascota.nombre
console.log(nombreMascota)

const {edad, comida} = mascota
console.log(edad)
console.log(comida)

//Objetos Anidados
const web = {
	nombre: "bluuweb",
  links: {
  	enlace: 'www.bluuweb.cl'
  },
  redesSociales:{
  	youtube:{
    	enlace:'www.youtube.com',
      nombre: 'YT'
    },
    facebook:{
    	enlace:'www.facebook.com',
      nombre: 'FB'
    }
  }  
}
const enlaceYT = web.redesSociales.youtube.enlace
console.log(enlaceYT)
//Destructuring
const {enlace, nombre} = web.redesSociales.youtube
console.log(enlace)
console.log(nombre)

//Fetch API
fetch('https://pokeapi.co/api/v2/pokemon')
	.then(res=> res.json())
    .then(data=>{
        //console.log(data.results)
        data.results.forEach(element => {
            console.log(element.name)
        })
    })
  .catch(error => console.log(error))

//Async / Await
const getPokemones = async() =>{
	try{
  	    const respuesta = await fetch('https://pokeapi.co/api/v2/pokemon')
        const data = await respuesta.json()
  	    console.log(data.results)
    }catch(error){
  	    console.log("error")
    }
} 
getPokemones()

//MAP vs Foreach
fetch('https://pokeapi.co/api/v2/pokemon')
	.then(res=> res.json())
  .then(data=>{
  	let nombrePokemones = []
    data.results.forEach(element => {
    	nombrePokemones.push(element.name)
    	//console.log(element.name)
    })
    //console.log(nombrePokemones)
  })
  .catch(error => console.log(error))

const getPokemones = async() =>{
	try{
  	const respuesta = await fetch('https://pokeapi.co/api/v2/pokemon')
    const data = await respuesta.json()
  	//data.results.map(pokemon => console.log(pokemon.name))
    const arrPokemones = data.results.map(pokemon=>pokemon.name)
    console.log(arrPokemones)
  }catch(error){
  	console.log("error")
  }
} 
getPokemones()

//FILTER
const getPokemones = async() =>{
	try{
  	const respuesta = await fetch('https://pokeapi.co/api/v2/pokemon')
    const data = await respuesta.json()
    //const arrPokemones = data.results.filter(pokemon=>pokemon.name === 'bulbasaur')
    const arrPokemones = data.results.filter(pokemon=>pokemon.name !== 'bulbasaur')
    console.log(arrPokemones)
  }catch(error){
  	console.log("error")
  }
} 
getPokemones()